<?php

/*
https://bitbucket.org/immanuelnoel/apipper
*/

require 'utils.php';
require 'helpers.php';

# Identify Resource / Response Type / Request Type / Parameters
$REQUEST = parse_request();

# Call identified resource with parameters
$APIResponse = call_resource($REQUEST["Resource"], $REQUEST["Parameters"]); 
 
# Return Response to browser - Format specifier
deliver_response($APIResponse, $REQUEST["Format"]);
 
?>