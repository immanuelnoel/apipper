<?php

/*
	Return Object / Array
*/
 
class weight {
	
	static function get($params){
		$params = getDictionary($params);
	
		$peterWeight = (object) array("Before"=>"135", "After"=>"137");
		
		if(isset($params['user']))
			$weight = array($params['user']=>$peterWeight);
		else
			$weight = array("Peter"=>$peterWeight, "Ben"=>"137", "Joe"=>"143");
		
		$response['weight'] = $weight;
		
		return $response;
	}
	
	static function post($params){
		$params = getDictionary($params);
		return "Successfully added weight (".$params['age'].") for user, ".$params['user'];
	}
	
	static function put($params){
		$params = getDictionary($params);
		return "Successfully modified weight (".$params['age'].") for user, ".$params['user'];
	}
	
	static function delete($params){
		$params = getDictionary($params);
		return "Successfully deleted weight for user, ".$params['user'];
	}
	
}
 
?>