<?php

/*
	Accepts only path params
	URLs are of the form, 
		GET: http://localhost/apipper/api-endpoints/age2/JohnDoe
		POST: http://localhost/apipper/api-endpoints/age2/JohnDoe/33
*/
 
class age2 {
	
	static function get($params){ 

		if(isset($params[0]))
			$age = array($params[0]=>'22');
		else
			$age = array("Peter"=>'28', "Ben"=>"37", "Joe"=>"43");
		
		$response['age'] = $age;
		return $response;
	}

	static function post($params){ 
		
		return "Successfully added age (".$params[1].") for user, ".$params[0];
	}

	static function put($params){ 
		
		return "Successfully modified age (".$params[1].") for user, ".$params[0];
	}

	static function delete($params){ 
		
		return "Successfully deleted age for user, ".$params[0];
	}
}
 
?>