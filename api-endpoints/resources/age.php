<?php

/*
	Needs to return Object / Array

	URLs are of the form, 
		GET: http://localhost/apipper/api-endpoints/age2/user/JohnDoe
		GET: http://localhost/apipper/api-endpoints/age2/?user=JohnDoe
		POST: http://localhost/apipper/api-endpoints/age2/user/JohnDoe/age/33
		POST: http://localhost/apipper/api-endpoints/age2/?user=JohnDoe&age=33
	
	getDictionary needs to be called explicitly to convert path parameters to key-value pairs
	
*/

class age {
	
	static function get($params){
		$params = getDictionary($params);
	
		if(isset($params['user']))
			$age = array($params['user']=>'22');
		else
			$age = array("Peter"=>'28', "Ben"=>"37", "Joe"=>"43");
		
		$response['age'] = $age;
		return $response;
	}
	
	static function post($params){
		$params = getDictionary($params);
		return "Successfully posted age (".$params['age'].") for user, ".$params['user'];
	}
	
	static function put($params){
		$params = getDictionary($params);
		return "Successfully put age (".$params['age'].") for user, ".$params['user'];
	}
	
	static function patch($params){
		$params = getDictionary($params);
		return "Successfully patched age (".$params['age'].") for user, ".$params['user'];
	}
	
	static function delete($params){
		$params = getDictionary($params);
		return "Successfully deleted age for user, ".$params['user'];
	}
	
}
 
?>