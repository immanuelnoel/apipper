<?php
 
/*
	Utility method for resource methods
	Method:getDictionary:
		- To generate key-value pairs from parameters
		Output
			- key-value pairs

*/

set_error_handler('exceptions_error_handler');

function exceptions_error_handler($severity, $message, $filename, $lineno) {
	if (error_reporting() == 0) {
		return;
	}
	if (error_reporting() & $severity) {
		throw new ErrorException($message, 0, $severity, $filename, $lineno);
	}
}

function getDictionary($params){
	
	$response = [];
	$i = 0;
	
	try {
		
		while($i < count($params)) {
			
			$key = $params[$i++];
			$value = $params[$i++];
			$response[$key] = $value;
			
		}
		
		return $response;
	}
	catch (Exception $e) {
		
		# Error in parsing Path Parameters. Revert to Query Parameters
		return $params;
	}
	
}

?>