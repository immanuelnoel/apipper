<?php

/*

	I N T E R N A L      M E T H O D S

*/ 

/*
Internal method
Reads parameters
*/ 
function parse_request(){
	
	# API resource
	$resource = Null;

	# Request Parameters
	$paramTypes = [];
	$params = Null;

	# Request Format - Defined as part of the context, as a path parameter - Defaults to JSON
	$format = "json";
	
	if (isset($_SERVER['PATH_INFO'])) {
	
		$path_info = $_SERVER['PATH_INFO'];
		
		$pos = strrpos($path_info, "/");
		if($pos !== false)
		{
			# Path parameters are defined
			# Identify resource, and parameters
			
			$params = explode("/",  $path_info);
			
			# Remove empty values, and reset array index
			$params = array_values(array_filter($params));
			
			# Remove resource from request parameters
			$resource = $params[0];
			unset($params[0]);
			
			# Reset array indexes
			$params = array_values($params);
			
			# Identify request format
			if(count($params) > 0){
			
				array_push($paramTypes, "path");
			
				$requestedResponseFormat = strtolower($params[0]);
				if($requestedResponseFormat == "xml" || $requestedResponseFormat == "json") {
					
					$format = $requestedResponseFormat;
					
					# Remove format from request parameters
					unset($params[0]);
					# Reset array indexes
					$params = array_values($params);
				
				}
				else
					$format = identifyAcceptType();
			}
			else
				$format = identifyAcceptType();
			
			# Assume query parameters if no more path parameter's are available
			if(count($params) == 0){
				$params = $_REQUEST;
			}
			
		} else {
			
			# Path parameters not defined. Assume Query parameters
			$resource = "";
			$params = [];
		}
		
	}

	$returnValue = [];
	$returnValue["Resource"] = $resource;
	$returnValue["Parameter-Type"] = $resource;
	$returnValue["Parameters"] = $params;
	$returnValue["Format"] = $format;
	
	return $returnValue;
}

/*
Internal method
Return type from Accept headers
*/
function identifyAcceptType(){
	
	if(isset($_SERVER['HTTP_ACCEPT'])) {
		$val = strpos(strtolower($_SERVER['HTTP_ACCEPT']),"json");
		if($val !== false) {
			return "json";
		}
		else {
			
			$val = strpos(strtolower($_SERVER['HTTP_ACCEPT']),"xml");
			if($val !== false)
				return "xml";
		}
		return "json";
	}
	return "json";
}


/*
Internal method
Call resource
*/
function call_resource($resource, $parameters){
	
	$response = [];
	
	# Call resource, handling exceptions
	try{

		if(isset($resource)) {
			
			# Call resource if resource file exists
			if(file_exists(dirname(__FILE__) . '/resources/'.$resource.'.php')){
			
				# Call requested resource
				include('resources/'.$resource.'.php');
				
				# Identify request type - Forwarded to implementation. Options / Head requests have auto-generated response, as a fallback
				$requestType = strtolower($_SERVER['REQUEST_METHOD']);
				
				# Call resource if request (type) method exists
				$methodToCall = $resource."::".$requestType;
				if(is_callable($methodToCall)) {
					
					$response['code'] = 1;
					$response['data'] = call_user_func($methodToCall, $parameters);
					
				} else {
					
					# If Options, and if implementation not provided by API, generate content
					if($requestType == "options"){
						
						$response['code'] = 1;
						
						$data = "";
						$availableMethods = get_class_methods($resource);
						
						for($i = 0; $i < count($availableMethods); $i++){
							$data .= strtolower($availableMethods[$i]);
							
							if($i != (count($availableMethods) - 1)) 
								$data .= ", ";
						}
						
						# List all methods defined in resource class
						$response['data'] = "Allow: ".$data." options head";
					
					} 
					# If Head, and if get is callable, generate content
					elseif($requestType == "head" && is_callable($resource."::get")){
					
						$response['code'] = 1;
						call_user_func($resource."::get", $parameters);
						
						// Send empty response
						$response['data'] = "";
						
					} else {
					
						$response['code'] = 2;
						$response['data'] = 'Unable to invoke resource method, '.$requestType;
					}
				}
				
			} else {
				$response['code'] = 2;
				$response['data'] = 'Unable to invoke resource, '.$resource;
			}
			
		
		} else {
			$response['code'] = 2;
			$response['data'] = 'Resource not specified';
		}
		
	} catch (Exception $e) {
		$response['code'] = 0;
		$response['data'] = 'Exception occurred: '. $e->getMessage();
	}

	return $response;
}
 
/*
Internal method
Returns response in XML or JSON formats
*/
function deliver_response($api_response, $format){
 	
	// Define API response codes and their related HTTP response
	$api_response_code = array(
		0 => array('HTTP Response' => 400, 'Message' => 'Bad Request'),
		1 => array('HTTP Response' => 200, 'Message' => 'Success'),
		2 => array('HTTP Response' => 404, 'Message' => 'Not Found')
	); 
	$api_response['status'] = $api_response_code[ $api_response['code'] ]['HTTP Response'];
	$api_response['message'] = $api_response_code[ $api_response['code'] ]['Message'];
 
    // Set HTTP Response
    header('HTTP/1.1 '.$api_response['status'].' '.$api_response_code[ $api_response['code'] ]['HTTP Response']);
 
    // Process different content types
    if( strcasecmp($format,'json') == 0 ){
 
        // Set HTTP Response Content Type
        header('Content-Type: application/json; charset=utf-8');
 
        // Format data into a JSON response
        $json_response = json_encode($api_response);
 
        // Deliver formatted data
        echo $json_response;
 
    }elseif( strcasecmp($format,'xml') == 0 ){
 
        // Set HTTP Response Content Type
        header('Content-Type: application/xml; charset=utf-8');
        
		$xml_response = '<?xml version="1.0" encoding="UTF-8"?>'."\n".
            '<response>'."\n";
		
		foreach($api_response as $x => $x_value) {
			$xml_response .= "\t".'<'.$x.'>'.parseValue($x_value).'</'.$x.'>'."\n";
		}
		
		$xml_response .= '</response>';
			
        // Deliver formatted data
        echo $xml_response;
 
    }
 
    // End script process
    exit;
}

/*
Internal method
Formats XML
*/
function parseValue($item){
	
	$response = "";
	// Treat as Array
	if(is_array($item))
	{
		foreach($item as $x => $x_value) {
			$response .= "\t".'<'.$x.'>'.parseValue($x_value).'</'.$x.'>'."\n";
		}
	}
	// Treat as Object
	elseif(is_object($item))
	{
		foreach($item as $x => $x_value) {
			$response .= "\t".'<'.$x.'>'.parseValue($x_value).'</'.$x.'>'."\n";
		}
	}
	// Treat as simple string
	else {
		$response = $item;
	}
	
	return $response;
}

 
?>